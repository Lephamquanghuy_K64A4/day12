<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");
$gioitinh = array(0 => 'Nam', 1 => 'Nữ');
$phankhoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$err = array();

$err["hoten"] = "<label class='empty'></label><br>";
$err["gioitinh"] = "<label class='empty'></label><br>";
$err["phankhoa"] = "<label class='empty'></label><br>";
$err["ngaysinh"] = "<label class='empty'></label><br>";
$err["hinhanh"] = "<label class='empty'></label><br>";

/*Hình ảnh
$filename = $_FILES["fileToUpload"]["name"];
$tempname = $_FILES["fileToUpload"]["tmp_name"];
$fileDestination = "Downloads".$filename;
move_uploaded_file($tempname,$filename);
accept='image/jpeg, image/png"
*/

//Thuộc tính
if (isset($_POST['submit'])) {
    session_start();
    $_SESSION['hoten'] = "";
    $_SESSION['gioitinh'] = "";
    $_SESSION['phankhoa'] = "";
    $_SESSION['phankhoakey'] = $phankhoa[$_POST["phankhoa"]];
    $_SESSION['ngaysinh'] = "";
    $_SESSION['diachi'] = "";
    $_SESSION['hinhanh'] = "";

    if (!file_exists('upload')) {
        mkdir('upload', 0777, true);
    }

    $filename = basename($_FILES["hinhanh"]["name"]); 
    $filetmpname = $_FILES['hinhanh']['tmp_name'];
    $dir = "upload/";
    $folder = $dir.$filename;
    $imageFileType = strtolower(pathinfo($folder,PATHINFO_EXTENSION));
    $expensions= array("jpeg","jpg","png");
    $ex = array("");

    $explode = explode('.',$filename);
    $file_ext=strtolower(end($explode));

    $destinationFileName = date('YmdHis').'.'.$file_ext;

    $a=0;
    //Kiem tra xem co dung dinh dang khong
    if(in_array($imageFileType,$ex) == false){
        if(in_array($imageFileType,$expensions)== false)
            $err['hinhanh'] = "<label class='error'>Đây không phải là file đúng định dạng png/jpeg/jpg.</label><br>";
        else{
            $folder = substr($folder, 0, strpos($folder, '.'));
            move_uploaded_file($filetmpname, $folder.'_'.$destinationFileName);
            $_SESSION["hinhanh"] = $folder.'_'.$destinationFileName;
        }
    }else{
        $a = 1;
    }

    //$check_valid_date = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";

    if (empty($_POST['hoten']))
        $err['hoten'] = "<label class='error'>Hãy nhập tên.</label><br>";
    else
        $_SESSION["hoten"] = $_POST["hoten"];
        
    if (empty($_POST['gioitinh']))
        $err['gioitinh'] = "<label class='error'>Hãy chọn giới tính.</label><br>";
    else
        $_SESSION["gioitinh"] =  $_POST["gioitinh"];

    if (empty($_POST['phankhoa']))
        $err['phankhoa'] = "<label class='error'>Hãy chọn phân khoa. <br /></label>";
    else
        $_SESSION["phankhoa"] = $_POST["phankhoa"];

    $date = $_POST['ngaysinh'];

    // ham check ngay sinh
    function isValid($date, $format = 'd/m/Y'){
        $dt = DateTime::createFromFormat($format, $date);
        return $dt && $dt->format($format) === $date;
    }

    if (empty($_POST['ngaysinh']))
        $err['ngaysinh'] = "<label class='error'>Hãy chọn ngày sinh. <br /></label>";
    else
        if (isValid($date) == false)
            $err['ngaysinh'] = "<label class='error'>Hãy chọn đúng định dạng ngày sinh (dd/mm/YYYY).</label><br>";        
        else
            $_SESSION["ngaysinh"] = $_POST["ngaysinh"];
    
    $_SESSION["diachi"] = $_POST["diachi"];

    //$_SESSION["hinhanh"] = $folder;

    if ($_SESSION["hoten"] != "" && $_SESSION["gioitinh"] != "" && $_SESSION["phankhoa"] != "" & $_SESSION["ngaysinh"] != "" && ($_SESSION["hinhanh"] != "" || $a ==1))
        header('Location: formsinhvien.php');
}
?>

<html>
    
    <head>
        <meta charset='UTF-8'>
        <link rel='stylesheet' href='sinhvien.css'>
    </head>

<body>

    <fieldset>
        <form method='post' enctype='multipart/form-data' action='sinhvien.php'>
            <?php
            echo $err["hoten"];
            echo $err["gioitinh"];
            echo $err["phankhoa"];
            echo $err["ngaysinh"];
            echo $err["hinhanh"];
            ?>

            <table>
                <tr>
                    <td class='td'><label>Họ và tên<span> * </span></label></td>
                    <td>
                        <?php 
                        echo "<input type='text' id='input' class='box' name='hoten' value='";
                        echo isset($_POST['hoten']) ? $_POST['hoten'] : '';
                        echo "'>"; ?>
                    </td>
                        
                </tr>

                <tr>
                    <td class='td'><label>Giới tính<span> * </span></label></td>
                    <td>
                    <?php
                        for ($i = 0; $i < count($gioitinh); $i++) {
                            echo
                                "<input type='radio' name='gioitinh' class='gioitinh' value='" . $gioitinh[$i] . "'";
                            echo (isset($_POST['gioitinh']) && $_POST['gioitinh'] == $gioitinh[$i]) ? " checked " : "";
                            echo "/>" . $gioitinh[$i];
                        }
                    ?>
                    </td>
                </tr>
                
                <tr>
                        <td class='td'><label>Phân khoa<span> * </span></label></td>
                        <td>
                            <select class='box' name='phankhoa'>
                            <?php
                                foreach ($phankhoa as $key => $value) {
                                    echo "<option";
                                    echo (isset($_POST['phankhoa']) && $_POST['phankhoa'] == $key) ? " selected " : "";
                                    echo " value='" . $key . "'>" . $value . "</option>";
                                }
                            ?>  
                            </select>
                        </td>
                    </tr>

                <tr>
                    <td class='td'><label>Ngày sinh <span>*</span> </label></td>
                    <td>
                        <?php
                        echo "<input type='text' id='ngaysinh' class='box' name='ngaysinh'  value='";
                        echo isset($_POST['ngaysinh']) ? $_POST['ngaysinh'] : '';
                        echo "'>"
                        ?></td>

                </tr>

                <tr>
                    <td class='td'><label>Địa chỉ</label></td>
                    <td>
                        <?php
                        echo "<input type='text' id='diachi' class='box' name='diachi' value='";
                        echo isset($_POST['diachi']) ? $_POST['diachi'] : '';
                        echo "'>" ?></td>
                </tr>

                <tr>
                    <td class='td'><label>Hình ảnh</label></td>
                    <td><input type='file' id ='hinhanh' name='hinhanh' value='";
                    echo "'></td>
                </tr>

            </table>

            <button name='submit' type='submit'>Đăng ký</button>
        </form>
    </fieldset>

</body>
</html>
