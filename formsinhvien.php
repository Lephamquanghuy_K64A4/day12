<?php
session_start();
date_default_timezone_set('Asia/Ho_Chi_Minh');
$servername = "localhost";
$username = "root";
$password = "";

if($_SERVER['REQUEST_METHOD']=='POST'){
    $name = $_SESSION["hoten"];

    $gender = $_SESSION["gioitinh"];
    if($gender == "Nam"){
        $gender = 1;
    }
    else $gender = 0;

    $faculty = $_SESSION["phankhoa"];

    #$birthday = $_SESSION["ngaysinh"];
    $ds = $_SESSION["ngaysinh"];
    $datetime = DateTime::createFromFormat('d/m/Y', $ds);
    $birthday = $datetime->format('Y-m-d g:i:s');

    $address = $_SESSION["diachi"];

    $avartar = $_SESSION["hinhanh"];

    try{
        $conn = new PDO("mysql:host=$servername;dbname=sinhvien", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $statements = 
            'CREATE TABLE IF NOT EXISTS `student`(
                `id` int(11) AUTO_INCREMENT NULL PRIMARY KEY,
                `name` varchar(250) NOT NULL,
                `gender` int(1) NOT NULL,
                `faculty` char(3) NOT NULL,
                `birthday` datetime NOT NULL,
                `address` varchar(250) DEFAULT NULL,
                `avartar` text DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';
        $conn -> exec($statements);

        $sql = $conn-> prepare("INSERT INTO student (name, gender, faculty, birthday, address, avartar) VALUES
        (:name, :gender, :faculty, :birthday, :address, :avartar)");
        #$change = "ALTER TABLE student modify column UserId int NOT NULL AUTO_INCREMENT";
        #$sql = $conn->prepare("UPDATE student SET id = id + 1 WHERE id =:id ");
        $sql->bindParam(':name',$name);
        $sql->bindParam(':gender',$gender);
        $sql->bindParam(':faculty',$faculty);
        $sql->bindParam(':birthday',$birthday);
        $sql->bindParam(':address',$address);
        $sql->bindParam(':avartar',$avartar);
        $sql->execute();
        header('Location: complete_regist.php');

    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}
?>

<html>
    <head>
        <meta charset='UTF-8'>
        <link rel='stylesheet' href='formsinhvien.css'>
    </head>

    <body>
        <fieldset>
            <form method='post' enctype='multipart/form-data' action='formsinhvien.php'>
                <table>
                    <tr>
                        <td class='td'><label>Họ và tên</label></td>
                        <td><label>
                            <?php echo "".$_SESSION["hoten"].""?>
                        </label></td>
                    </tr>
                    <tr>
                        <td class='td'><label>Giới tính</label></td>
                        <td><label>
                        <?php echo "".$_SESSION["gioitinh"].""?>
                        </label></td>
                    </tr>
                    <tr>
                        <td class='td'><label>Phân khoa</label></td>
                        <td><label>
                        <?php echo "".$_SESSION["phankhoakey"].""?>
                        </label></td>
                    </tr>
                    <tr>
                        <td class='td'><label>Ngày sinh</label></td>
                        <td><label>
                            <?php echo "".$_SESSION["ngaysinh"].""?>
                        </label></td>
                    </tr>

                    <tr>
                        <td class='td'><label>Địa chỉ</label></td>
                        <td><label>
                            <?php echo "".$_SESSION["diachi"].""?>
                        </label></td>
                    </tr>

                    <tr>
                        <td class='td'><label>Hình ảnh</label></td>
                        <td><label>
                            <?PHP
                                echo "<img src = ".$_SESSION["hinhanh"]." width='100' height='50'>" ?>
                            </label></td>
                    </tr>
                </table>
                <button name='submit' type='submit'>Xác nhận</button>
            </form>
        </fieldset>
    </body>
<html>
